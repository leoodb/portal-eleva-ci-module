terraform {
  required_version = "~> 0.12.0"
}

# ----------------------------------------------------------------------------------------------------------------------
# CI Module for Portal Eleva
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket" "source_bucket" {
  bucket = "${var.application}-${var.environment}-artifact"
  acl    = "private"


  versioning {
    enabled = var.versioning
  }

  tags = var.tags

}


resource "aws_iam_role" "build_role" {
  name = "${var.application}-${var.environment}-codebuildrole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy" "build_policy" {
  role = "${aws_iam_role.build_role.name}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeDhcpOptions",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.source_bucket.arn}",
        "${aws_s3_bucket.source_bucket.arn}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_codebuild_project" "portal" {
  name         = "${var.application}-${var.environment}"
  description  = "Build project for Portal Eleva Application"
  service_role = "${aws_iam_role.build_role.arn}"

  artifacts {
    type      = var.artifact_type
    location  = aws_s3_bucket.source_bucket.id
    name      = var.artifact_name
    packaging = var.artifact_packaging_type
  }

  environment {
    compute_type                = var.environment_compute_type
    image                       = var.environment_image
    type                        = var.environment_type
    image_pull_credentials_type = var.environment_credentials
  }

  source {
    type      = var.source_type
    buildspec = var.source_buildspec
    location  = var.source_location
    auth {
      type = var.source_auth_type
    }
    report_build_status = "true"
  }

  tags = var.tags

}
