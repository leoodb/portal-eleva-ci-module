# Módulo Terraform - AWS - Continuous Integration for Portal Eleva

Módulo do Terraform para provisionamento de recursos do Code Build para o Portal Eleva.

## Uso

```hcl
module "portal-eleva-ci-module" {
  source = "git::ssh://git@gitlab.com/mandic-labs/terraform/modules/aws/portal-eleva-ci-module.git?ref=v0.0.1"

  application = "portal-eleva"
  environment = "development"
  region = "us-east-1"

  artifact_type = "S3"
  artifact_name = "portal-eleva.zip"
  artifact_packaging = "ZIP"

  environment_compute_type = "BUILD_GENERAL1_MEDIUM"
  environment_image = "941660504087.dkr.ecr.us-east-1.amazonaws.com/pe-codebuild-backend:latest"
  environment_type = "WINDOWS_CONTAINER"
  environment_credentials = "CODEBUILD"

  source_type = "BITBUCKET"
  source_buildspec = "BuildCfg\buildspec.yaml"
  source_location = "https://cechinbr@bitbucket.org/elevaeducacao/portal-eleva.git"
  source_auth_type = "OAUTH"

  }
```

## Recursos provisionados

- Projeto do CodeBuild
- Bucket S3 para os artefatos

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| application | Nome da aplicação envolvida | `any` | n/a | yes |
| artifact\_name | Nome dado a saída do build e.g portal-eleva.zip | `any` | n/a | yes |
| artifact\_packaging\_type | Tipo de empacotamento do artefato e.g ZIP | `any` | n/a | yes |
| artifact\_type | Onde será salvo a saída do build e.g S3 | `any` | n/a | yes |
| environment | Nome do ambiente envolvido | `any` | n/a | yes |
| environment\_compute\_type | Tipo de máquina que ira subir a imagem docker para realizar o build | `any` | n/a | yes |
| environment\_credentials | Tipo de credenciais que serão utilizadas para acessar a imagem docker | `any` | n/a | yes |
| environment\_image | A imagem que será utilizada no container docker para realizar o build | `any` | n/a | yes |
| environment\_type | O tipo de ambiente que executará esse build e.g WINDOWS\_CONTAINER | `any` | n/a | yes |
| region | Nome da região da AWS em que os recursos serão provisionados | `any` | n/a | yes |
| source\_auth\_type | Tipo de autenticação a sua base de código | `any` | n/a | yes |
| source\_buildspec | Nome do arquivo de buildspec / Path para onde ele se encontra na base de código | `any` | n/a | yes |
| source\_location | Link para acesso ao seu repositório de código | `any` | n/a | yes |
| source\_type | Onde está o código da aplicação e.g BITBUCKET | `any` | n/a | yes |
| tags | Map de tags usadas em todos os recursos | `map` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| codebuild\_artifact\_store | Nome do bucket S3 onde serão salvos os artefatos do Code Build |
| codebuild\_project\_arn | ARN do projeto do codebuild |
| codebuild\_project\_id | ID/Nome do projeto do codebuild |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Licença

Copyright © 2020 Mandic Cloud Solutions. Todos os direitos reservados.
