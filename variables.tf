
variable "application" {
  description = "Nome da aplicação envolvida"
}

variable "environment" {
  description = "Nome do ambiente envolvido"
}

variable "region" {
  description = "Nome da região da AWS em que os recursos serão provisionados"
}

varialbe "versioning" {
  description = "Ativar o versionamento no bucket de artefatos da aplicação ?"
  default     = true
}

variable "artifact_type" {
  description = "Onde será salvo a saída do build e.g S3"
}

variable "artifact_name" {
  description = "Nome dado a saída do build e.g portal-eleva.zip"
}

variable "artifact_packaging_type" {
  description = "Tipo de empacotamento do artefato e.g ZIP"
}

variable "environment_compute_type" {
  description = "Tipo de máquina que ira subir a imagem docker para realizar o build"
}

variable "environment_image" {
  description = "A imagem que será utilizada no container docker para realizar o build"
}

variable "environment_type" {
  description = "O tipo de ambiente que executará esse build e.g WINDOWS_CONTAINER"
}

variable "environment_credentials" {
  description = "Tipo de credenciais que serão utilizadas para acessar a imagem docker"
}

variable "source_type" {
  description = "Onde está o código da aplicação e.g BITBUCKET"
}

variable "source_buildspec" {
  description = "Nome do arquivo de buildspec / Path para onde ele se encontra na base de código"
}

variable "source_location" {
  description = "Link para acesso ao seu repositório de código"
}

variable "source_auth_type" {
  description = "Tipo de autenticação a sua base de código"
}

variable "tags" {
  description = "Map de tags usadas em todos os recursos"
  default     = {}
}
