
output "codebuild_project_id" {
  description = "ID/Nome do projeto do codebuild"
  value       = aws_codebuild_project.portal.id
}

output "codebuild_project_arn" {
  description = "ARN do projeto do codebuild"
  value       = aws_codebuild_project.portal.arn
}

output "codebuild_artifact_store" {
  description = "Nome do bucket S3 onde serão salvos os artefatos do Code Build"
  value       = aws_s3_bucket.source_bucket.id
}
